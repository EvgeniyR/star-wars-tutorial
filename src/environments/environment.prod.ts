export const environment = {
  production: true,
  ROOT_API: 'https://swapi.dev/api/',
  RESOURCES: {
    FILMS: 'films/',
    PEOPLE: 'people/',
    PLANETS: 'planets/',
    SPECIES: 'species/',
    STARSHIPS: 'starships/',
    VEHICLES: 'vehicles/'
  }
};
