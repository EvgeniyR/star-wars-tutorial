import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { InjectionToken, NgModule } from '@angular/core';
import { environment } from 'src/environments/environment';
import { FilmsEndpointService } from './endpoints/films-endpoint/films-endpoint.service';

export const API_HOST: InjectionToken<string> = new InjectionToken('APP API HOST');

export const FILMS_RESOURCE: InjectionToken<string> = new InjectionToken('FILM RESOURCE');
export const PEOPLE_RESOURCE: InjectionToken<string> = new InjectionToken('PEOPLE RESOURCE');
export const PLANETS_RESOURCE: InjectionToken<string> = new InjectionToken('PLANETS RESOURCE');
export const SPECIES_RESOURCE: InjectionToken<string> = new InjectionToken('SPECIES RESOURCE');
export const STARSHIPS_RESOURCE: InjectionToken<string> = new InjectionToken('STARSHIPS RESOURCE');
export const VEHICLES_RESOURCE: InjectionToken<string> = new InjectionToken('VEHICLES RESOURCE');

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [
    HttpClientModule
  ],
  providers: [
    {
      provide: API_HOST,
      useValue: environment.ROOT_API
    },
    {
      provide: FILMS_RESOURCE,
      useValue: environment.RESOURCES.FILMS
    },
    {
      provide: PEOPLE_RESOURCE,
      useValue: environment.RESOURCES.PEOPLE
    },
    {
      provide: PLANETS_RESOURCE,
      useValue: environment.RESOURCES.PLANETS
    },
    {
      provide: SPECIES_RESOURCE,
      useValue: environment.RESOURCES.SPECIES
    },
    {
      provide: STARSHIPS_RESOURCE,
      useValue: environment.RESOURCES.STARSHIPS
    },
    {
      provide: VEHICLES_RESOURCE,
      useValue: environment.RESOURCES.VEHICLES
    },
    FilmsEndpointService,
  ]
})
export class AppAPiModule { }
