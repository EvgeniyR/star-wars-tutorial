import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GetRequestConfiguration } from '../requests/get-request-configuration.model';

// @Injectable()
export abstract class AbstractApiEndpoint {
  constructor(private rootApiPath: string, private endpoint: string, private httpClient: HttpClient) { }

  public get<T>(request: GetRequestConfiguration): Observable<T> {
    return this.httpClient.get(
      `${this.getFinalUrl()}${request.slug}`,
      {
        headers: request.headers,
        responseType: request.responseType as 'json'
      }) as Observable<T>;
  }

  public post<T>(payload: any): Observable<T> {
    return this.httpClient.post(this.getFinalUrl(), payload) as Observable<T>;
  }

  public put<T>(payload: any): Observable<T> {
    return this.httpClient.put(this.getFinalUrl(), payload) as Observable<T>;
  }

  public patch<T>(payload: any): Observable<T> {
    return this.httpClient.patch(this.getFinalUrl(), payload) as Observable<T>;
  }

  public delete<T>(): Observable<T> {
    return this.httpClient.delete(this.getFinalUrl()) as Observable<T>;
  }

  protected getFinalUrl(): string {
    const url: string = `${this.rootApiPath}${this.endpoint}`
    return url;
  }
}
