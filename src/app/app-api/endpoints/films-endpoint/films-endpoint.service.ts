import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_HOST, FILMS_RESOURCE } from '../../app-api.module';
import { GetRequestConfiguration } from '../../requests/get-request-configuration.model';
import { AbstractApiEndpoint } from '../abstract-api-endpoint';
import { AllFilmsResponse, Film } from '../response-model/get-response.model';

@Injectable()
export class FilmsEndpointService extends AbstractApiEndpoint {

  constructor(@Inject(API_HOST) apiHost: string, @Inject(FILMS_RESOURCE) endpoint: string, httpClient: HttpClient) {
    super(apiHost, endpoint, httpClient);
  }

  public getAllFilms(): Observable<AllFilmsResponse> {
    const request: GetRequestConfiguration = new GetRequestConfiguration('');
    return this.get(request);
  }

  public getFilm(filmId: number | string): Observable<Film> {
    const request: GetRequestConfiguration = new GetRequestConfiguration(filmId.toString());
    return this.get(request);
  }
}
