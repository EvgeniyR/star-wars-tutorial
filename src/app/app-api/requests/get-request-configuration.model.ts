import { HttpHeaders } from '@angular/common/http';

export class GetRequestConfiguration {
  constructor(private _slug: string, private _requestHeaders: HttpHeaders = new HttpHeaders(), private _responseType: string = 'json') { }

  get slug(): string {
    return this._slug;
  }

  get headers(): HttpHeaders {
    return this._requestHeaders;
  }

  get responseType(): string {
    return this._responseType;
  }

  public addHeader(name: string, value: string | string[]): void {
    this._requestHeaders = this._requestHeaders.append(name, value);
  }
}
