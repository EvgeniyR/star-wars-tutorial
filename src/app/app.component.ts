import { Component, OnInit } from '@angular/core';
import { tap } from 'rxjs';
import { FilmsEndpointService } from './app-api/endpoints/films-endpoint/films-endpoint.service';
import { AllFilmsResponse } from './app-api/endpoints/response-model/get-response.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'star-wars-app';
  constructor(private filmsService: FilmsEndpointService) { }

  ngOnInit(): void {

  }
}
