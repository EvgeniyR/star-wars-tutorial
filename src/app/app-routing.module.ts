import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './components/main-page/main.component';
import { SplashScreenComponent } from './components/splash-screen/splash-screen.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    pathMatch: 'full'
  },
  {
    path: 'welcome',
    component: SplashScreenComponent
  },
  { path: 'overview', loadChildren: () => import('./modules/public/public/public.module').then(m => m.PublicModule) },
  { path: 'member', loadChildren: () => import('./modules/member/member/member.module').then(m => m.MemberModule) },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
