import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppAPiModule } from './app-api/app-api.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SplashScreenComponent } from './components/splash-screen/splash-screen.component';
import { UrlSerializer } from '@angular/router';
import { AppUrlSerializer } from './components/url-serializer';
import { MainComponent } from './components/main-page/main.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MainPageRightMenuComponent } from './components/main-page/components/main-page-right-menu/main-page-right-menu.component';

@NgModule({
  declarations: [
    AppComponent,
    SplashScreenComponent,
    MainComponent,
    MainPageRightMenuComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppAPiModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatButtonModule,
  ],
  providers: [
    // {
    //   provide: UrlSerializer,
    //   useClass: AppUrlSerializer
    // }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
