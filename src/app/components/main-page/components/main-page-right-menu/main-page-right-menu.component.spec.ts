import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainPageRightMenuComponent } from './main-page-right-menu.component';

describe('MainPageRightMenuComponent', () => {
  let component: MainPageRightMenuComponent;
  let fixture: ComponentFixture<MainPageRightMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainPageRightMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainPageRightMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
