import { animate, animation, AnimationReferenceMetadata, AnimationTriggerMetadata, keyframes, state, style, trigger } from '@angular/animations';

export const creditsTransitionAnimation: AnimationReferenceMetadata = animation([
  animate('15s ease-in', keyframes([
    style({ transform: 'perspective(2000px) rotateX(50deg) translateY(0%)', top: '100%' }),
    style({ transform: 'perspective(2000px) rotateX(50deg) translateY(-100%)', top: '0%' })
  ]))
]);

export const mainBannerScaleAnimation: AnimationReferenceMetadata = animation([
  animate('{{time}} ease-in-out', keyframes([
    style({ transform: 'scale3d(0,0,1)' }),
    style({ transform: 'scale3d(1,1,1)' })
  ]))
], { params: { time: '8s' } });

export const saberScaleAnimation: AnimationReferenceMetadata = animation([
  animate('2.5s {{delay}} ease-in', keyframes([
    style({ filter: 'opacity(0)' }),
    style({ filter: 'opacity(1)' })
  ]))
], { params: { delay: mainBannerScaleAnimation.options?.params?.['time'] } });

const navigationAnimationDelay: string =
  (parseFloat(saberScaleAnimation.options?.params?.['delay']) + 5).toString() + 's';

export const navigationAnimation: AnimationReferenceMetadata = animation([
  animate('2.5s {{delay}} ease-in', keyframes([
    style({ filter: 'opacity(0)' }),
    style({ filter: 'opacity(1)' })
  ]))
], { params: { delay: navigationAnimationDelay } });

export const CreditsTrigger: AnimationTriggerMetadata = trigger('credits', [
]);

export const MainBannerTrigger: AnimationTriggerMetadata = trigger('bannerChild', [
  state('in', style({ transform: 'scale3d(0,0,1)' })),
  state('stable', style({ transform: 'scale3d(1,1,1)' })),
]);

export const SaberTrigger: AnimationTriggerMetadata = trigger('saberChild', [
  state('in', style({ filter: 'opacity(0)' })),
  state('stable', style({ filter: 'opacity(1)' })),
]);

export const NavigationTrigger: AnimationTriggerMetadata = trigger('navigationChild', [
  state('in', style({ filter: 'opacity(0)' })),
  state('stable', style({ filter: 'opacity(1)' })),
]);
