import { AnimationEvent, trigger, state, style, transition, animate, sequence, query, keyframes, animateChild, useAnimation, group, stagger } from '@angular/animations';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { fromEvent, concatMap, defer, tap, delay, map } from 'rxjs';
import { mainBannerScaleAnimation, saberScaleAnimation, creditsTransitionAnimation, CreditsTrigger, MainBannerTrigger, SaberTrigger, NavigationTrigger, navigationAnimation } from './animation/animation-splash-screen';

@Component({
  selector: 'app-splash-screen',
  templateUrl: './splash-screen.component.html',
  styleUrls: ['./splash-screen.component.scss'],
  animations: [
    trigger('animateChildContainer', [
      transition('in => stable', [
        query('@bannerChild', style({ transform: 'scale3d(0,0,1)' })),
        query('@saberChild', style({ filter: 'opacity(0)' })),
        query('@navigationChild', style({ filter: 'opacity(0)' })),

        query('@credits', [
          useAnimation(creditsTransitionAnimation)
        ], { optional: true }),

        query('@bannerChild', [
          useAnimation(mainBannerScaleAnimation)
        ], { optional: true }),

        query('@saberChild', [
          useAnimation(saberScaleAnimation)
        ], { optional: true }),

        query('@navigationChild', [
          useAnimation(navigationAnimation)
        ], { optional: true })
      ])
    ]),
    CreditsTrigger,
    MainBannerTrigger,
    SaberTrigger,
    NavigationTrigger
  ]
})
export class SplashScreenComponent implements OnInit, AfterViewInit {
  @ViewChild('audioPlayer', { static: true }) audioPlayer!: ElementRef<HTMLAudioElement>;

  public animationFrame = 'in';
  public source = 'https://ia801203.us.archive.org/13/items/13BinarySunsetAlternate/01%20Star%20Wars%20Main%20Title%20And%20The%20Arrival%20At%20Naboo.mp3';
  constructor() { }

  ngOnInit(): void {
  }

  public listenStart(event: AnimationEvent): void {
    console.log('%c LISTEN ANIMATION ', 'color:white;background:navy', event);
  }

  public listenDone(event: AnimationEvent): void {
    console.log('%c LISTEN ANIMATION DONE ', 'color:white;background:green', event);
  }

  ngAfterViewInit(): void {
    fromEvent(this.audioPlayer.nativeElement, 'canplaythrough').pipe(
      map((event) => {
        this.audioPlayer.nativeElement.muted = true;
        (event.target as HTMLAudioElement)?.play()
          .then(
            () => this.audioPlayer.nativeElement.muted = false,
            (er) => { console.log('%c ERRR', 'color:green', er); });
        return event
      }),
      delay(1500)
    ).subscribe(
      (event) => {
        this.animationFrame = 'stable';
      }
    );
  }

}
