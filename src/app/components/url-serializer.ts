import { DefaultUrlSerializer, UrlSerializer, UrlTree } from '@angular/router';

export class AppUrlSerializer implements UrlSerializer {
  static OUTLETS = ['header'];
  #defaultSerializer = new DefaultUrlSerializer();

  parse(url: string): UrlTree {
    AppUrlSerializer.OUTLETS.forEach(outletName => {
      const reg = new RegExp('/(' + outletName + ')/([^\/]*)');
      url = url.replace(reg, '$1/($1:$2)');
    });
    return this.#defaultSerializer.parse(url);
  }

  serialize(tree: UrlTree): string {

    let url = this.#defaultSerializer.serialize(tree);
    console.log('URL TREE', url);
    AppUrlSerializer.OUTLETS.forEach(outletName => {
      const reg = new RegExp('\\(' + outletName + ':([^\/]*)\\)');
      url = url.replace(reg, '$1');
    });
    return url;
  }
}
